import axios from "axios";

const KEY = process.env.REACT_APP_KEY;

export const config = (search) => ({
  params: {
    part: "snippet",
    maxResults: 16,
    q: search,
    key: KEY,
  },
});

export const initialize = axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
});
