import React, { useState, useEffect } from "react";
import * as youtubeApi from "../apis/youtube";
import termsKeyword from "../helpers/data";
import SearchBar from "./SearchBar";
import VideoList from "./VideoList";
import VideoDetail from "./VideoDetail";

const App = () => {
  const [videos, setVideos] = useState([]);
  const [selectedVideo, setSelectedVideo] = useState(null);

  const onTermSubmit = async (term) => {
    const response = await youtubeApi.initialize.get(
      "/search",
      youtubeApi.config(term)
    );

    setVideos(response.data.items);
    setSelectedVideo(response.data.items[0]);
  };

  const onVideoSelect = (video) => {
    setSelectedVideo(video);
    window.scrollTo(0, 0);
  };

  useEffect(() => {
    onTermSubmit(termsKeyword());
  }, []);

  return (
    <div className="app ui container">
      <h1 className="ui huge header white">Video Browsers</h1>
      <SearchBar onTermSubmit={onTermSubmit} />
      <VideoDetail video={selectedVideo} />
      <VideoList onVideoSelect={onVideoSelect} videos={videos} />
    </div>
  );
};

export default App;
