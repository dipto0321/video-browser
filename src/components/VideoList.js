import React from "react";
import VideoItem from "./videoItem";

const VideoList = ({ onVideoSelect, videos }) => {
  const listItemsClasses = `doubling stackable four column ui grid cards mt`;
  const videoItems = videos.map((video) => {
    return (
      <VideoItem
        key={video.id.videoId}
        onVideoSelect={onVideoSelect}
        video={video}
      />
    );
  });
  return <div className={listItemsClasses}>{videoItems}</div>;
};

export default VideoList;
