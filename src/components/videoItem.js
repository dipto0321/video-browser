import React from "react";

const videoItems = ({ video, onVideoSelect }) => {
  return (
    <section onClick={() => onVideoSelect(video)} className="ui card pointer">
      <img
        className="image w-100"
        src={video.snippet.thumbnails.medium.url}
        alt={video.snippet.title}
      />
      <div className="content">
        <p className="header">{video.snippet.title}</p>
      </div>
    </section>
  );
};

export default videoItems;
