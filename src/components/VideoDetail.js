import React from "react";
import Loader from "react-loader-spinner";
import ReactPlayer from "react-player";
import moment from "moment";
const VideoDetail = ({ video }) => {
  if (!video) {
    return <Loader type="Bars" color="#00BFFF" height="100" width="100" />;
  }
  const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`;
  return (
    <div>
      <div className="ui embed">
        <ReactPlayer url={videoSrc} controls={true} />
      </div>
      <div className="ui segment">
        <h4 className="header">{video.snippet.title}</h4>
        <span className="ui label">
          {moment(video.snippet.publishedAt).fromNow()}
        </span>
        <p>{video.snippet.description}</p>
      </div>
    </div>
  );
};

export default VideoDetail;
