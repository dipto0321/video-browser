export default () => {
  const terms = [
    "motivation",
    "technologies",
    "4th generation industry",
    "mars",
    "meditation",
    "football",
    "mind game",
    "iq test",
    "brain power",
    "Psychology",
    "Fun",
    "Comedy",
    "python",
    "self-realization",
    "spiritual science",
  ];
  return terms[Math.floor(Math.random() * terms.length)];
};
