# video-browser

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Netlify Status](https://api.netlify.com/api/v1/badges/1ed3dfd5-b5da-445d-ac4b-28c0992b027a/deploy-status)](https://app.netlify.com/sites/video-browser-react/deploys)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![code linter: eslint](https://img.shields.io/badge/linter-eslint-blue)](https://github.com/eslint/eslint)

> A simple video browsing and playing site. Built with react. [Live Demo](https://video-browser-react.netlify.com/)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Table of Contents

- [video-browser](#video-browser)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
      - [`npm start`](#npm-start)
      - [`npm test`](#npm-test)
      - [`npm run build`](#npm-run-build)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install
For installing this project please clone first

```
git clone <repository link>
```

## Usage

In the project directory, you can run:

#### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

#### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

#### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Maintainers

[@dipto0321](https://github.com/dipto0321)

## Contributing

PRs accepted.



## License

MIT © 2019 Dipto Karmakar
